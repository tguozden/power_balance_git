%  super figura muestra tuti
load('../data/demand.dat')
load('../data/wind_solar_cfs.dat')
demand=1e-3*demand(1:size(pvs,1));  % cut two years, convert to MW to GW
mat=reshape(demand,24,size(pvs,1)/24);



clf
h3=subplot(2,2,3);
imagesc(mat);
tiquis= [16:30.4:731];
imaxis=axis();
xticklabel=[
datestr("2015-02-01",'%Y-%m');  " ";" ";" "
datestr("2015-06-01",'%Y-%m');  " ";" ";" "
datestr("2015-10-01",'%Y-%m');  " ";" ";" "
datestr("2016-02-01",'%Y-%m');  " ";" ";" "
datestr("2016-06-01",'%Y-%m');  " ";" ";" "
datestr("2016-10-01",'%Y-%m');  " ";" ";" "];
set(gca,'xtick',tiquis,'xticklabel',xticklabel)

aux=caxis;
ylabel('local hour')
xlabel('date')
h2=subplot(2,2,2)

set(gca,'visible','off')
cbar=colorbar('location','EastOutside','ylabel','demand [GW]','ytick',[10 14 18 22]);
caxis(aux)


h4=subplot(2,2,4)
plot(mean(mat'),[24:-1:1],'o-')
%  axis([.5 1.5 .5 24.5 ])
%  text(.9,20,'daily average')
%  set(gca,'yaxislocation','right','ylabel','daily average')
grid on
xlabel('hourly avg. [GW]')
set(gca,'yticklabel','','xtick',[12,14,16,18])

h1=subplot(2,2,1)

plot(mean(reshape(demand,24,size(pvs,1)/24)))
axis([.5 size(pvs,1)/24+.5 10 20])
set(gca,'xtick',(tiquis-imaxis(1))/((imaxis(2)-imaxis(1)))*size(pvs,1)/24+.5,'xticklabel','' ,'ytick',[10,15,20] )

ylabel('daily avg. [GW]')
 
p1=get(h1,'Position');
p2=get(cbar,'Position');
p3=get(h3,'Position');
p4=get(h4,'Position');

p3(3)*=2;
p3(4)*=.8;
p1(4)=p3(4)*.6;
p2(4)=p1(4);
p4(4)=p3(4);

p4(1)=p3(1)+p3(3)*1.02;
p4(3)/=3;
p2(1)=p4(1);
p1(3)=p3(3);
%  p2(3)=p4(3);
p1(2)=(p3(2)+p3(4)*1.1);
p2(2)=p1(2);
set(h1,'Position',p1);
set(cbar,'Position',p2);
set(h3,'Position',p3);
set(h4,'Position',p4);

print ../img/heatplot_demand.eps -F:12 -color


%print ../img/dmh_demand.pdf -F:14
%system('pdfcrop ../img/heatplot_demand.pdf')
%system('rm ../img/heatplot_demand.pdf')


