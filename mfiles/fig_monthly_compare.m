% figure to compare mean monthly and hourly values between wrf and merra at 100 meters above ground, using all sites average

load('../data/wind_data_wrf.dat' )

load('../data/merra_data.dat')
dt=(datenum("2015-01-15 00:00:00")-datenum("1979-12-31 21:00:00"))*24; % difference between initial times
seriev=seriev([1:size(seriew,1)]+dt,:);  % same period as wrf


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
twrf=[0:size(seriew,1)-1]'/24+datenum('2015-01-01 00:00:00');
xhour =datevec(twrf)(:,4);
xmonth=datevec(twrf)(:,2);
xyear=datevec(twrf)(:,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%month mean
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vmonth=[];
for j=1:12
    jj=find((xmonth==j) & (xyear>2015));
    vmonth(j,:)=mean(seriev(jj,:));%./mean(seriev);
end
vmonthw=[];%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
for j=1:12
    jj=find((xmonth==j) & (xyear>2015));
    vmonthw(j,:)=mean(seriew(jj,:));%./mean(seriew);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%intraday mean
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vday=[];
for i=1:24
     ih=find((i==(xhour+1))  );
%       ih=find(i==(xhour+1) );
   vday(i,:)=mean(seriev(ih,:));
end
vdayw=[];
for i=1:24
     ih=find((i==(xhour+1))  );
%       ih=find(i==(xhour+1) );
   vdayw(i,:)=mean(seriew(ih,:));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% multiplot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clf
h1=subplot(3,1,1)
plot([1:12],mean(vmonthw'),'linewidth',2,'-o',[1:12],mean(vmonth'),'linewidth',2,'-x')
axis([.75 12.25 5.5 11])
l1=legend("WRF","MERRA",'location','southeast')
xlabel('month')
ylabel('av. speed [m/s]')
h2=subplot(3,1,2)
plot([0:23],mean(vdayw'),'linewidth',2,'-o',[0:23],mean(vday'),'linewidth',2,'-x')
axis([-.5 23.5 5.5 11])
xlabel('hour')
ylabel('av. speed [m/s]')
p2=get(h2,'position');
p1=get(h1,'position');
p1(3)*=.7;
p2(3)*=.7;
pp1=p1;

p1(2)=p2(2)+p2(4)*1.4;
pl1=get(l1,'position');
pl1(2)=(p1(2)+.02*p1(4));
pl1(1)=p1(1)+p1(3)-pl1(3)*1.02;
%  pl2=get(l2,'position');
%  pl2(1)=pl2(1)*.75;
%  set(l1,'position',pl1)
set(h1,'position',p1,'ytick',[6 8 10])
set(h2,'position',p2,'ytick',[6 8 10]);
set(l1,'position',pl1)
%  set(l2,'position',pl2)

% print ../img/compara_mensuales.eps -color

%system('pdfcrop ../img/compara_mensuales.pdf')
%system('rm   ../img/compara_mensuales.pdf')

