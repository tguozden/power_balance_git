%% Optimize residual demand
%
% script to generate data for the famous red curve and bar plot
%
%%

% Copyright (C) 2019 Tomás Guozden
% Copyright (C) 2019 Juan Pablo Carbajal
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

clear all
close all

% Put this to false if you do not want plots of the progress
showprogress = true;

%% Load data
% Load all the required data
%

CF     = []; # hourly Capacity Factors for all sites
powCap = []; # installed power capacities of each site
fname = datafile ('wind_cfs.mat');
printf ('Loading %s\n', fname);
load (fname);
CF       = [CF hourlyCF];
powCap   = [powCap; powerCap];
nsamples = rows (CF);

fname = datafile ('solar_cfs.mat');
printf ('Loading %s\n', fname);
load (fname);
% keep solar data that matches wind (provided by wind_cfs.mat)
% WARNING: this assummes initial time and sampling equal to wind_cfs.mat.
CF                = [CF hourlyCF(1:nsamples,:)];
powCap            = [powCap; powerCap];
[nsamples nsites] = size (CF);

fname = datafile ('demand.dat');
printf ('Loading %s\n', fname);
load (fname);
% keep only demand data that matches sites(provided by [wind|solar]_cfs.mat)
% WARNING: this assummes initla time and sampling equal to CF.
demand = demand(1:nsamples);

Pcurr       = CF * powCap;               % current power generation
Presid0     = demand - Pcurr;            % residual demand without additonal
                                         % capacity
Totaldemand = sum (demand);              % Sum of all demand
Fraccurr    = sum (Pcurr) / Totaldemand; % Fraction of current power to demand

%% Optimize residual demand
% Select the method to test
optimize_power = @optimpower_qp;

% This initilizes the optimizer and gives a first guess for the coefficients
a = optimize_power (3e3*8760, ones (nsites, 1), CF, Presid0);

totalener     = [.01:.005:1.5].' * sum (demand); # constrain renewables total energy  
ntp            = length (totalener);
PSigma         = zeros (ntp, 1);                # Std. deviation of res. demand
powerCap_addit = zeros (ntp, nsites);           # optimized addit. capacities

% Prepare plots
stdPresid0 = std (Presid0); % Initial std. dev of residual power
if showprogress
  figure (1);
    hp = plot (NA, NA,'-o');
    line ([0 1], stdPresid0 * [1 1]);
    xlabel ('Total power to demand ratio');
    ylabel ('Residual demand Dispersion');
    text (0.5, stdPresid0 + 10, 'Dispersion without additional power');
  figure (2);
    ha = bar (zeros (nsites, 1));
    xlabel ('Group #');
    ylabel ('Additional power capacity');
%  figure (3);
%    [c x] = hist (Presid0 , 32);
%    hpr = bar (x, c);
%    xlabel ('Residual power');
%    title ('Histogram of residual power time series');
%    axis ([-20e3 20e3 0 1600]);
%    axis manual;
endif

for ip = 1:ntp
  [a sigma]            = optimize_power (totalener(ip), a);
  PSigma(ip,:)         = sigma;
  powerCap_addit(ip,:) = a.';

  if showprogress
    Paddit    = CF * a;            % additonal power
    Presid    = Presid0 - Paddit;  % residual power
    Fractotal = sum (Paddit) / Totaldemand + Fraccurr;

    figure(1)
      xdata = [get(hp, 'xdata') Fractotal];
      set (hp, 'xdata', xdata, 'ydata', PSigma(1:ip));
      axis tight;
    figure (2);
      set (ha, 'ydata', a);
      axis([1 67 0 3500]);
 %   figure 3;
 %     [c x] = hist(Presid, 32);
 %     set (hpr, 'xdata', x, 'ydata', c);

    printf ("Total power: %.2f, Max residual demand: %.2f\n", ...
      totalener(ip), max (Presid));
    fflush (stdout);
  end%if
end%for
if showprogress
  figure (1); grid on;
end%if

optim_ren = optimize_power (100, ones (nsites, 1), CF, zeros(size(CF,1),1));
   %%%   tiene que cumplirse que sum(CF*optim_ren)=0.1*sum(demand);
optim_ren *= 0.1*sum(demand)/sum(CF*optim_ren);   %rescaling so that capacity equals 10 percent of share

metadata = units = struct();
metadata.totalener = ['total power optimized for'];   %totalener == mean(CF*powerCap_addit')
metadata.PSigma = ['std. deviation of residual power for each total power'];
metadata.CF = ['hourly capacity factor series of sites'];
metadata.powerCap_addit = ['optimized additional capacity of each site, ' ...
                 'one row per value of total power'];
metadata.powCap = ['installed power capacities of each site'];
metadata.optim_ren = ['best renewables combination']

units.powCap = 'MW';
units.optim_ren = 'MW';
units.totalener = units.Psigma = units.powerCap_addit = 'MWhr';
units.CF = '';

fname = datafile ('optimcapacity_raw.mat');
save ('-v7', fname, fieldnames(metadata){:}, 'metadata', 'units');
printf ('Data saved to %s\n', fname);
