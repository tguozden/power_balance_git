%figure 4a
load('../data/optimcapacity_raw.mat') % load results of optimization

load('../data/demand.dat') % for demand
demand=demand(1:size(CF,1));  % demand series is larger than two years



% these are three rows of the PSigma data for aproximately 3, 6 and 9 GW of added cap.
na=19;
nb=39;
nc=59;

%(check total additional capacity in each case):
sum(powerCap_addit(na,:))
sum(powerCap_addit(nb,:))
sum(powerCap_addit(nc,:))



mat_cap=repmat(powCap,1,299)+powerCap_addit';
mean_cf = mean((CF*mat_cap)./sum(mat_cap));
   %%%remember:   tatalener == sum(CF*powerCap_addit')

fixaxis=[-2000 22000 0 10];
nhist = [-10000:500:25000]+250;  %  32 ;
   h4=subplot(4,1,4)
   hist(demand-CF*powCap-(CF*mat_cap)(:,nc),nhist,100)  
   axis(fixaxis)
   $posc=get(gca,"position");
   set(gca,"xlabel",'residual power [MW]')
   set(gca,"ylabel",'freq [%]','xtick',[-10000:5000:25000])
   text(-1450,4,'D: share 40%')
   
   h3=subplot(4,1,3)
   hist(demand-CF*powCap-(CF*mat_cap)(:,nb),nhist,100)

   axis(fixaxis)
   set(gca,"xticklabel",[])
   set(gca,"ylabel",'freq [%]','xtick',[-10000:5000:25000])
   text(-1450,4,'C: share 30%')
   
   h2=subplot(4,1,2)
   hist(demand-CF*powCap-(CF*mat_cap)(:,na),nhist,100) 
   
   axis(fixaxis)
   set(gca,"xticklabel",[])
   set(gca,"ylabel",'freq [%]','xtick',[-10000:5000:25000])
   text(-1450,4,'B: share 20%')
   
   h1=subplot(4,1,1)
   hist( demand -CF*powCap,nhist,100)  
   
   axis(fixaxis)
   set(gca,"xticklabel",[])
   set(gca,"ylabel",'freq [%]','xtick',[-10000:5000:25000])
   text(-1450,4,'A: share 10%')
   

p1=get(h1,'Position');
p2=get(h2,'Position');
p3=get(h3,'Position');
p4=get(h4,'Position');

b=.03;

p3(2)=p4(2)+p4(4)+b;

set(h3,'Position',p3);

p2(2)=p3(2)+p3(4)+b;
set(h2,'Position',p2);

p1(2)=p2(2)+p2(4)+b;
set(h1,'Position',p1);

print ../img/pp.eps -F:12 -color 
system('epstool --copy -b  ../img/pp.eps ../img/fig_raw_hists.eps')
system('rm  ../img/pp.eps')

%print ../img/fig_raw_hists.pdf -F:12
%system('pdfcrop ../img/fig_raw_hists.pdf')
%system('rm ../img/fig_raw_hists.pdf')

