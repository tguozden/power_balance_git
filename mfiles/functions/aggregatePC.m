## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-01-12

## -*- texinfo -*-
## @defun {@var{} =} aggregatePC (@var{}, @var{})
##
## @seealso{}
## @end defun

function PCa = aggregatePC (w, PC, s = [], wpdf = [], wlim = [])

  if isempty (wpdf)
    # Gaussian
    nw   = size (w, 1);
    if isempty (s)
      s    = 0.6 + 0.05 * w;
    else
      s = s(w);
    endif

    wpdf = @(x) normpdf (repmat (x, nw, 1), ...
                         repmat (w, 1, size (x, 2)), ...
                         repmat (s, 1, size (x, 2)));
    if isempty (wlim)
      wlim = [max(min(w - 4 * s),0) max(w + 4 * s)];
    endif

  endif

  if isempty (wlim)
    tol = 1e-6;
    wlim(1) = min (w - fzero (@(x)wpdf(x) - tol, [-300, 1]));
    wlim(2) = max (w - fzero (@(x)wpdf(x) - tol, [wlim(1), 300]));
  endif

  PCG   = @(n) wpdf (n) .* PC (n);
  PCa   = quadrature (PCG, wlim);
endfunction

function q = quadrature (func, lim)
   x = linspace (lim(1), lim(2), 1e3);
   q = trapz (x, func (x), 2);
endfunction

%!demo
%! PC  = @(x)pc_pw(x, [3.5 17 25 1].');
%! nw  = 100;
%! w   = linspace (0, 60, nw).';
%! PCa = aggregatePC (w, PC);
%!
%! s    = 0.6 + 0.2 * w;
%! k    = (s ./ w).^(-1.086);
%! l    = w ./ gamma (1 + 1./k);
%! wpdf = @(x) wblpdf (repmat (x, nw, 1), ...
%!                     repmat (l, 1, size (x,2)), ...
%!                     repmat (k, 1, size (x,2)));
%! wlim = [0, 500];
%! PCa2 = aggregatePC (w, PC, wpdf, wlim);
%!
%! plot (w, PCa,'-;Gaussian;', w, PCa2, '-;Weibull;', w, PC(w), '-;Delta;');
%! axis tight
%! xlabel ('Mean wind speed [m/s]')
%! ylabel ('Generated power / (N Pr) [a.u.]')
