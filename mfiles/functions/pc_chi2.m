## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {} {@var{pow} =} pc_chi2 (@var{v}, @var{p})
## Power curve to convert wind speed @var{v} (m/s) to power @var{pow} (Watts).
##
## Empirical model derived from data. Uses a reflected chi2cdf function as basic shape.
##
## The parameters are as follows.
## @var{p}(1): root of the function or speed of activation (cut-in speed).
## @var{p}(2): argument scaling.
## @var{p}(3): point of reflection or speed of saturation.
## @var{p}(4): power scaling.
##
## @end deftypefn

function p = pc_chi2 (v, param)
 p = param(4,:) .* ...
     max (0, param(1,:) - chi2cdf (param(2,:) .* ( param(3,:) - v ), 3));
endfunction

%!demo
%! param = [0.9;0.7;12.3;3926];
%! v     = linspace (0,25,100).';
%! plot (v, pc_chi2 (v, param));
%! axis tight
%! grid on
%! xlabel ("Wind speed [m/s]");
%! ylabel ("Power [W]");
