## Copyright (C) 2019 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2019-04-16

## -*- texinfo -*-
## @defun {@var{PC} =} aggregatedpowercurves (@var{wmax}, @var{dw}=0.25)
## @defunx {@var{PC} =} aggregatedpowercurves (@dots{}, turbine_pc)
## Compute aggregated power curves
##
## The aggregated power curves are comuted in the wind speed
## interval [0, @var{wmax}]. The resolution of the interval is defined by
## @var{dw}, with a default value of 0.25.
##
## The aggregated power curves are interpolated using a piece-wise spline
## and the polanomial structure is returned in @var{PC}.
## The results can be used to convert wind speed signals to capacity factors
## using @code{ppval (PC{i}, w)}.
##
## The optional input argument @var{turbine_pc} is a cell of functions
## defining individual power curves.
## If not provided, the function uses the curves defined in the files
## @asis{'v*_martin.dat'}, in the data folder.
##
## @seealso{datafile, aggregatePC}
## @end defun

function PCa = aggregatedpowercurves (Wmax, dw=0.25, PC=[])

  if isempty (PC)

    persistent PC_
    if isempty (PC_)
      fname = {'v112_martin.dat', 'v117_martin.dat', 'v126_martin.dat'};
      fname = datafile (fname);
      nt    = numel (fname);          % number of turbine power curve classes
      PC    = cell(nt, 1);
      for i=1:nt
        printf ('Loading %s\n', fname{i});
        pcdata = load (fname{i});
        % Create power curve fuction from data
        pcdata(:,2) = pcdata(:,2) / max (pcdata(:,2)); % normalize to [0 1]
        polypc      = interp1 (pcdata(:,1), pcdata(:,2), 'linear', 'pp');
        vcut        = max (pcdata(:,1));
        % This is the individual power curve with v-cut given by data
        PC_{i}      = @(x) ifelse(x <= vcut, ppval (polypc, x), 0.0);
      end%for
    end%if
    PC = PC_;

  else % individual power curves were given

    if !iscell (PC)
      % wrapp into cell
      PC = {PC};
    end%if
    % Check that PCs are functions
    if any (cellfun (@isnumeric, PC))
      error ('Octave:invalid-input-arg', 'Power curves should be functions')
    end%if

  end%if

  nt = numel (PC);
  
  % allocate
  PCa = cell(nt, 1); % Aggregated power curves as polynomial for ppval
  % interval of definition
  W = (0:dw:Wmax).';
  for it = 1:nt    % iterate over turbine power curves
      % This aggregates individual PCs with a Guassian distribution
      % on their parameters and creates and inteprolating polynomial
      PCa{it} = interp1 (W, aggregatePC (W, PC{it}), 'spline', 'pp');
  end%for over turbine power curves

end%function
