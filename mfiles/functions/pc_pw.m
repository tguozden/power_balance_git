## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-01-12

## -*- texinfo -*-
## @defun {@var{} =} pc_pw (@var{}, @var{})
## Power curve to convert wind speed @var{v} (m/s) to power @var{pow} (Watts).
##
## Piecewise model.
##
## The parameters are as follows.
## @var{p}(1): root of the function or speed of activation (cut-in speed).
## @var{p}(2): argument scaling.
## @var{p}(3): point of reflection or speed of saturation.
## @var{p}(4): power scaling.
##
## @seealso{}
## @end defun

function p = pc_pw (w, params)
  vc = params(1,:);
  vr = params(2,:);
  vf = params(3,:);
  Pr = params(4,:);

  tf_1 = (w >= vr) & (w <= vf);
  tf_0 = (w <  vc) | (w >  vf);

  vc3     = vc.^3;
  p       = (w.^3 - vc3) ./ (vr.^3 - vc3);
  p(tf_1) = 1;
  p(tf_0) = 0;

  p .*= Pr;
endfunction

%!demo
%! vc = [3 4];
%! vr = [11 17];
%! vf = 25;
%! [Vc Vr] = meshgrid (vc, vr);
%! params = [Vc(:) Vr(:)].';
%! params(3,:) = vf;
%! params(4,:) = 1e3;
%! w = linspace (0, 30, 100).';
%! p = pc_pw (w, params);
%! plot(w, p);
%! axis tight
%! xlabel ('Wind speed [m/s]');
%! ylabel ('Generated power [W]');
