%% Copyright (C) 2019 Juan Pablo Carbajal
%%
%% This program is free software; you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
%% Created: 2019-02-28

function fullfname = datafile (fname)
  % FULLFNAME = DATAFILE (FNAME)
  % Returns the full path to a file named FNAME
  %
  % A full file path is generating attaching to FNAME the path to the data folder.
  % The latter is stored in the evniroment varibale DATAPATH.
  %
  % FNAME can be a cell of strings, in which case FULLFNAME is also a cell
  %
  % See also FULLFILE

  persistent datapath
  if isempty (datapath)
    datapath = getenv ('DATAPATH');
  end%if

  notcell = false;
  if ~iscell (fname)
    notcell = true;
    fname   = {fname};
  end%if

  fullfname = fname;
  for i=1:numel(fname)
    fullfname{i} = fullfile (datapath, fname{i});
  end%for

  if notcell
    fullfname = fullfname{1};
  end%if

end%function
