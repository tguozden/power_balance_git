%% Copyright (C) 2019 Tomás Guozden
%% Copyright (C) 2019 Juan Pablo Carbajal
%%
%% This program is free software; you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
%% Created: 2019-03-05

function [a sigma] = optimpower_fminsearch (totalpower, a, omega=[], Presid=[], nIter=2)

  persistent meancf K_cfrd
  if !isempty (omega)
    meancf = mean (omega); % temporal mean capacity factor of each group
    % residual demand after current proyects
    % covariance between between groups capacity factor and residual demand
    K_cfrd = cov ([omega, Presid]);
  endif

  cost = @(x) powervariance (x, totalpower, K_cfrd, meancf);
  for i = 1:nIter  % iterate to check convergence (?)
    a     = fminsearch (cost, a);
    % (rescale total capacity and eliminate posible negative values)
    a     = abs (a) * totalpower / (meancf * a);
    sigma = sqrt (cost (a));
  end
end%function

function y = powervariance (x, pprop, Kgdh, mg)
  xx = abs (x); % fminsearch tries negative values
  % Mean power of additional capacity is P_{mean, add capacity} = sum(mg*xx) = pprop [MW].
  % This equations fixes the mean power of additional capacity, and thus its
  % total energy contribution.
  xx = xx * pprop / (mg * xx);
  xx = [xx; -1];
  y  = xx.' * Kgdh * xx; % variance of residual power
end%function
