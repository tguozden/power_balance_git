%% Copyright (C) 2017 - Juan Pablo Carbajal
%%
%% This program is free software; you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

%% -*- texinfo -*-
%% @deftypefn {} {@var{} =} power_qp (@var{}, @var{})
%%
%% @seealso{}
%% @end deftypefn

function [a sigma] = optimpower_qp (totalener, a, omega=[], Presid=[], MaxIter=200)

  persistent H q A c
  if !isempty (omega)
    [nT N] = size (omega);

    % Linear part
    totener = sum (omega);                       % Integrated total energy sum (omega) * a;
    % Quadratic part
    Sigmaww = cov (omega);                   % Total power variance var (omega * a)
    Sigmadw = cov (Presid, omega);

    q  = - 2 * Sigmadw;          % Objective function linear coefficients
    H  = 2 * Sigmaww;            % Objective function quadratic coefficients
    A  = sum (omega);           % Constraints coefficients
    c  = var (Presid);
  end%if
  N = length (a);

  b  = totalener;            % Right-hand side value of the constraint
  LB = zeros (N,1);            % Lower bounds

  a0  = rand (N,1);
  a0 *= totalener / sum (a0);

  [a, obj, infocod, lambda] = qp (a0, H, q, A, b, LB, [],...
                                  struct('MaxIter',MaxIter));
  if infocod.info
    warning ("Non-zero QP info code!")
  endif
  sigma = sqrt (obj + c);

endfunction
