%  generate data for parks_cap_map.py script 
% need coordinates, wheter sites are wind or solar parks, initial capacity, and differential capacity at +3, +6 and +9 GW 

% Wind site locations
load (datafile ('wind_data_wrf.dat'), 'wind_sites', 'seriew');
coordw      = cell2mat(wind_sites(:,6:7));   % position of wind sites lat,long

% Solar site locations
load (datafile ('sun_data.dat'), 'coords');

coord=[coordw;coords];


powCap = []; # installed power capacities of each site
fname = datafile ('wind_cfs.mat');
printf ('Loading %s\n', fname);
load (fname);
powCap   = [powCap; powerCap];

fname = datafile ('solar_cfs.mat');
printf ('Loading %s\n', fname);
load (fname);
% keep solar data that matches wind (provided by wind_cfs.mat)
% WARNING: this assummes initial time and sampling equal to wind_cfs.mat.
powCap            = [powCap; powerCap];


fname = datafile ('optimcapacity_raw.mat');
printf ('Loading %s\n', fname);



%%%
%%generate text file
%iswind
iswind=ones(size(coordw,1),1);
issolar=2*ones(size(coords,1),1);

%sum(powerCap_addit')(14)
%ans =  3093.2
%sum(powerCap_addit')(30)
%ans =  6085.1
%sum(powerCap_addit')(47)
%ans =  9068.4

mat=[[iswind;issolar],powCap, powerCap_addit'(:,14),  powerCap_addit'(:,30)- powerCap_addit'(:,14), powerCap_addit'(:,47) - powerCap_addit'(:,30), coord   ];


fname = datafile ('parks_map_data.txt');
save ('-ascii', fname, 'mat');
printf ('Data saved to %s\n', fname);
