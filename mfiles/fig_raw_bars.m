%figure 5
load('../data/optimcapacity_raw.mat') % load results of optimization
load('../data/demand.dat') % for demand
demand=demand(1:size(CF,1));  % demand series is larger than two years



% these are three rows of the PSigma data for aproximately 3, 6 and 9 GW of added cap.
na=19;
nb=39;
nc=59;
%(check total additional capacity in each case):
sum(powerCap_addit(na,:))
sum(powerCap_addit(nb,:))
sum(powerCap_addit(nc,:))


    
mat_cap=repmat(powCap,1,299)+powerCap_addit';
mean_cf = mean((CF*mat_cap)./sum(mat_cap));
   %%%remember:   tatalener == sum(CF*powerCap_addit')
totalpower=sum(powerCap_addit)+sum(powCap);
share=(sum(CF*powCap)+totalener)/sum(demand)*100;
xpoints = [0:422:16000]';


fixaxis2=[0.5 67.5 0 1300];
e2=fixaxis2(end);
fixaxis3=[0.5 67.5 0 950];
e3=fixaxis3(end);
fixaxis4=[0.5 67.5 0 750];
e4=fixaxis4(end);
fixaxis5=[0.5 67.5 0 450];
e5=fixaxis5(end);


    h5=subplot(5,1,5);
    bar(optim_ren);
    axis(fixaxis5)
    set (gca, "xgrid", "off",'ytick',[0:200:1400],'xtick',[5:10:67]);
    set(gca,"xlabel",'site number')
    set(gca,"ylabel",'[MW]')
    text(20,300,'E: renewables best configuration')
    
   h4=subplot(5,1,4);
   b4=bar(powerCap_addit(nc,:)-powerCap_addit(nb,:));
   axis(fixaxis4)
   set (gca, "xgrid", "off",'ytick',[0:200:1400],'xtick',[5:10:67]);
   $posc=get(gca,"position");
   set(gca,"xticklabel",[])
   set(gca,"ylabel",'[MW]')
   text(20,500,'D: 30% to 40\%')
   
   h3=subplot(5,1,3);
   bar(powerCap_addit(nb,:)-powerCap_addit(na,:))
   axis(fixaxis3)
   set (gca, "xgrid", "off",'ytick',[0:200:1400],'xtick',[5:10:67]);
   set(gca,"xticklabel",[])
   set(gca,"ylabel",'[MW]')
   text(20,600,'C: 20% to 30\%')
   
   h2=subplot(5,1,2);
   bar(powerCap_addit(na,:))
   axis(fixaxis2)
   set (gca, "xgrid", "off",'ytick',[0:200:1400],'xtick',[5:10:67]);
   set(gca,"xticklabel",[])
   set(gca,"ylabel",'[MW]')
   text(20,600,'B: 10% to 20\%')
   
   h1=subplot(5,1,1);
   bar(powCap)
   axis(fixaxis5)
   set (gca, "xgrid", "off",'ytick',[0:200:1400],'xtick',[5:10:67]);
   
   set(gca,"xticklabel",[])
   set(gca,"ylabel",'[MW]')
   text(37,300,'A:  (Tables 3 and 4, ~3.4 GW) ')
   
   
p1=get(h1,'Position');
p2=get(h2,'Position');
p3=get(h3,'Position');
p4=get(h4,'Position');
p5=get(h5,'Position');

b=.01;
p4(2)=p5(2)+p5(4)+b;
p4(4)*=e4/e5;
set(h4,'Position',p4);

p3(2)=p4(2)+p4(4)+b;
p3(4)*=e3/e5;

set(h3,'Position',p3);

p2(2)=p3(2)+p3(4)+b;
p2(4)*=e2/e5;
set(h2,'Position',p2);

p1(2)=p2(2)+p2(4)+b;
set(h1,'Position',p1);


%set(h1,'Position',p1);
%set(cbar,'Position',p2);
%set(h3,'Position',p3);
%
   
print ../img/fig_raw_bars.eps -F:12 -color

%print ../img/fig_raw_bars.pdf -F:12
%system('pdfcrop ../img/fig_raw_bars.pdf')
%system('rm ../img/fig_raw_bars.pdf')
