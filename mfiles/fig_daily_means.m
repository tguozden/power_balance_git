load('../data/optimcapacity_raw.mat') % load results of optimization

load('../data/demand.dat') % for demand
demand=demand(1:size(CF,1));  % demand series is larger than two years


plot(
sum(reshape(demand,24,17544/24)')/mean(sum(reshape(demand,24,17544/24)')),'','linewidth',3,'color','black',
sum(reshape(CF(:,1),24,17544/24)')/mean(sum(reshape(CF(:,1),24,17544/24)')),'','linewidth',2,
sum(reshape(CF(:,2),24,17544/24)')/mean(sum(reshape(CF(:,2),24,17544/24)')),'','linewidth',2,
sum(reshape(CF(:,4),24,17544/24)')/mean(sum(reshape(CF(:,4),24,17544/24)')),'','linewidth',2,
sum(reshape(CF(:,3),24,17544/24)')/mean(sum(reshape(CF(:,3),24,17544/24)')),'','linewidth',2)


xlabel('local time')
ylabel('hourly averages')
legend('demand','Wind site #1', 'Wind site #2', 'Wind site #3','Wind site #4','location','north')

print ../img/fig_daily_means.eps -F:14 -color
%system('pdfcrop ../img/fig_daily_means.pdf')
%system('rm ../img/fig_daily_means.pdf')
