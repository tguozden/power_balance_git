%figure 4a
load('../data/optimcapacity_raw.mat') % load results of optimization

load('../data/demand.dat') % for demand
demand=demand(1:size(CF,1));  % demand series is larger than two years



% these are three rows of the PSigma data for aproximately 3, 6 and 9 GW of added capacity
na=19;  %14;
nb=39;  %30;
nc=59;   %  47;
%(check total additional capacity in each case):
sum(powerCap_addit(na,:))
sum(powerCap_addit(nb,:))
sum(powerCap_addit(nc,:))


mat_cap=repmat(powCap,1,299)+powerCap_addit';
mean_cf = mean((CF*mat_cap)./sum(mat_cap));
   %%%remember:   tatalener == sum(CF*powerCap_addit')
totalpower=sum(powerCap_addit)+sum(powCap);
share=(sum(CF*powCap)+totalener)/sum(demand)*100;
xpoints = [0:422:16000]';

%unicap=ones(size(powCap))/67;
%mean(CF*powCap)/mean(demand)*100+sum(CF*unicap)/sum(demand)*xpoints*100,std(repmat(demand,1,38)-CF*powCap-(CF*unicap %)*xpoints')','k-.;(using uniform distribution);',...

plot(...
0,std(demand),'.','markersize',20, ...
mean(CF*powCap)/mean(demand)*100,                     std(demand-CF*powCap),'.','markersize',20, ...
[mean(CF*powCap)/mean(demand)*100;share],             [std(demand-CF*powCap);PSigma(:)],'-;optimized distr.;', ...
share([na,nb,nc]),                                       PSigma([na,nb,nc]),'.k', 'markersize',20, ...
sum(CF*powCap)/sum(demand)*xpoints/sum(powCap)*100,std(repmat(demand,1,38)-(1/sum(powCap)*CF*powCap)*xpoints')','k--;(using projected distribution);')

axis([0 45 2300 3000])
legend('location','northwest')
ylabel('residual power dispertion [MW]')
xlabel('renewable energy share [%]')
text(.5,2440,'installed and projected',"color","red")
text(5.,2410,'(3.4 GW)',"color","red")
text(.25,2320,'electricity load dispertion',"color","blue")

text(21,PSigma(na),'(3.4+4) GW')
text(31,PSigma(nb),'(3.4+7.7) GW')
text(38,PSigma(nc)-40,'(3.4+11) GW')
%  grid on
print ../img/opt_curve.eps -F:14 -color

%print ../img/opt_curve.pdf -F:18
%system('p   dfcrop ../img/opt_curve.pdf')
%system('rm ../img/opt_curve.pdf')
