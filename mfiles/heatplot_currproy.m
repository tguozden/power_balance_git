%% Generate Figure 3: Power series
% Generate plot of power series from years 2015 and 2016 of wind and solar
% facilities
%
%%

% Copyright (C) 2019 Tomás Guozden
% Copyright (C) 2019 Juan Pablo Carbajal
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.


fname = datafile ('wind_solar_cfs.mat'); % generate this file with wind2power.m
printf ('Loading %s\n', fname);
load (fname);

mat=1e-3*reshape(pvs*pot_vs,24,size(pvs,1)/24);
serie=1e-3*pvs*pot_vs;

clf
h3=subplot(2,2,3);
imagesc(mat);

tiquis= [16:30.4:731];
%  tiquis=[datenum("2015-02-01") datenum("2015-06-01") datenum("2015-10-01")
%  datenum("2016-02-01") datenum("2016-06-01") datenum("2016-10-01") ]-xdemand(1);
%  tiquis=get(gca,'xtick');
imaxis=axis();
xticklabel=[
datestr("2015-02-01",'%Y-%m');  " ";" ";" "
datestr("2015-06-01",'%Y-%m');  " ";" ";" "
datestr("2015-10-01",'%Y-%m');  " ";" ";" "
datestr("2016-02-01",'%Y-%m');  " ";" ";" "
datestr("2016-06-01",'%Y-%m');  " ";" ";" "
datestr("2016-10-01",'%Y-%m');  " ";" ";" "];
set(gca,'xtick',tiquis,'xticklabel',xticklabel)

%  set(gca,'xtick',tiquis,'xticklabel',datestr(tiquis+xdemand(1),'%m-%Y'))

aux=caxis;
ylabel('local hour')
xlabel('date')
h2=subplot(2,2,2);

set(gca,'visible','off')
cbar=colorbar('location', 'EastOutside','ylabel','[GW]','ytick',[1,2,3]);
caxis(aux)


%  set(cbar,'ytick',[.5 1.5 2.5])
%  set(cbar,'xtick',[.8,1,1.2,1.4])
%  set(cbar,'xticklabel',["-20\%";"0\%";"+20\%";"+40\%"])

%  xlabel(cbar,'hourly wind speed')

h4=subplot(2,2,4);
plot(mean(mat'),[24:-1:1],'o-')
axis([1.3 1.8 .5 24.5 ])
%  text(.9,20,'daily average')
%  set(gca,'yaxislocation','right','ylabel','daily average')
grid on
xlabel('hourly av. [GW]')
set(gca,'yticklabel','','xtick',[1.3, 1.8])

h1=subplot(2,2,1);

plot(mean(reshape(pvs*pot_vs,24,size(pvs,1)/24))/1e3)
axis([.5 size(pvs,1)/24+.5 ])
set(gca,'xtick',(tiquis-imaxis(1))/((imaxis(2)-imaxis(1)))*size(pvs,1)/24+.5,'xticklabel','' ,'ytick',[0.5,1.5,2.5] )

ylabel('daily av. [GW]')
%  text(.7,1.1,'monthly average')
%  title('monthly average')

p1=get(h1,'Position');
p2=get(cbar,'Position');
p3=get(h3,'Position');
p4=get(h4,'Position');

p3(3)*=2;
p3(4)*=.8;
p1(4)=p3(4)*.6;
p2(4)=p1(4);
p4(4)=p3(4);

p4(1)=p3(1)+p3(3)*1.02;
p4(3)/=3;
p2(1)=p4(1);
p1(3)=p3(3);
%  p2(3)=p4(3);
p1(2)=(p3(2)+p3(4)*1.1);
p2(2)=p1(2);
set(h1,'Position',p1);
set(cbar,'Position',p2);
set(h3,'Position',p3);
set(h4,'Position',p4);


print ../img/heatplot_currproy.eps -F:12 -color

%print (1, '../img/heatplot_currproy.pdf');
%system('pdfcrop ../img/heatplot_currproy.pdf')
%system('rm ../img/heatplot_currproy.pdf')
