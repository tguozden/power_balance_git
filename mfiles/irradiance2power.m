%% Convert solar irradiance to power
%
%%

% Copyright (C) 2019 Tomás Guozden
% Copyright (C) 2019 Juan Pablo Carbajal
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

clear all
close all

pkg load statistics

%% Load data
% Load all the required data
%
fname = datafile ('sun_data.dat');
printf ('Loading %s\n', fname);
load (fname);

%%
%  generate hourly capacity factor matrix of solar sites
%
hourlyCF = cf_sol;
powerCap = pot_sol;

metadata = units  = struct();
metadata.hourlyCF = ['hourly capacity factor series for solar sites,' ...
                   'a two-year period starting 2015-01-01 00:00hs UTC-3.\n'
                   'Rows are samples and columns solar sites.'];
units.hourlyCF    = 'adimensional';

% Some values in powerCap are actually projected capacities (as of 2018).
% We consider them as installed because they are already committed.
metadata.powerCap = ['column vector containing the capacity installed' ...
                     ' at solar sites [in GW]. The length of this vector' 
                     ' matches the number of sites.'];
units.powerCap    = 'GW';

fname = datafile ('solar_cfs.mat');
save ('-v7', fname, 'hourlyCF', 'powerCap', 'metadata', 'units');
printf ('Data saved to %s\n', fname);
