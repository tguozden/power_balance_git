load('../data/optimcapacity_raw.mat') % load results of optimization

load('../data/demand.dat') % for demand
demand=demand(1:size(CF,1));  % demand series is larger than two years

bw=bar([1:46],corr(CF,demand)(1:46))
set (bw, "facecolor", "b")
hold on
bs=bar([47:67],corr(CF,demand)(47:67))
grid on;
set (bs, "facecolor", "y")
hold off


axis([.5,67.5,-.12,.14])
xlabel('site number')
ylabel('correlation with demand')
    
print  ../img/fig_corr_sites_demand.eps -F:14 -color
#system('epspdf ../img/fig_corr_sites_demand.epsc')
#system('pdfcrop ../img/fig_corr_sites_demand.pdf')
#system('rm ../img/fig_corr_sites_demand.epsc')
#system('rm ../img/fig_corr_sites_demand.pdf')
