ccd=pwd;
load('../data/arauco_nov2016.dat') % need permission from CAMMESA
tmed_arau=datenum("31-Oct-2016")+[0:size(pmed,1)-1]'/24;
parau_med=pmed;

%  load simulation and measured data
load('../data/chimb_sim_med_15_16.dat'); % this file is distributed previous permission of CAMMESA
cfchi_sim=cfsim;
cfchi_med=pmed/capacity_MW;  %measured capacity factor series
tchi=datenum('2015-01-01')+[0:size(cfchi_sim,1)-1]'/24;
ichi_s=find(abs(tmed_arau(1)-tchi)<.001);
ichi=ichi_s:(ichi_s+size(tmed_arau,1)-1);

load('../data/rawson_loma_15-16.dat');% this file is distributed previous permission of CAMMESA
plr_med=pmed;

load('../data/merra_data.dat')

load('../data/wind_solar_cfs.mat')

%simulation start on'2015-01-15'
twrf=[0:size(pvs,1)-1]/24+datenum('2015-01-15');

%  we choose time period to be that of arauco measurements
iwrf_s=find(abs(tmed_arau(1)-twrf)<.001);
iwrf=iwrf_s:(iwrf_s+size(tmed_arau,1)-1);
tw=twrf(iwrf);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
h(1)=subplot(5,1,1);
plot(parau_med/27,'',pvs(iwrf,2)) 
axis([0 748,0,1.1]);
set(gca,'ytick',[0,.5,1],'xticklabel',[],'xtick',[5 10 15 20 25 ]*24)
text(1,1.5,"A: Arauco wind park, hourly CF",'horizontalalignment', 'left')
set(gca,'ylabel','CF')
%lh=legend('measured','simulated')
%legend(lh,'boxoff')
%pl=get(lh,'position');
%pl(2)-=.017;
%set(lh,'position',pl)

h(2)=subplot(5,1,2);
plot(pmed(iwrf+14*24,1)/77.4,'',pvs(iwrf,27)) % -14 porque wrf comienza el 15 de enero y las mediciones son desde el 1 de enero.
axis([0 748,0,1.1]);
set(gca,'xticklabel',[],'ytick',[0,.5,1],'xtick',[5 10 15 20 25 ]*24)
text(1,1.5,"B: Rawson wind park, hourly CF",'horizontalalignment', 'left')
set(gca,'ylabel','CF')


h(3)=subplot(5,1,3);
plot(pmed(iwrf+14*24,2)/51,'',pvs(iwrf,25)) % -14 porque wrf comienza el 15 de enero y las mediciones son desde el 1 de enero.
axis([0 748,0,1.1]);
set(gca,'xticklabel',[],'ytick',[0,.5,1],'xtick',[5 10 15 20 25 ]*24)
text(1,1.5,"C: Loma Blanca wind park, hourly CF",'horizontalalignment', 'left')
set(gca,'ylabel','CF')

h(4)=subplot(5,1,4);
plot(cfchi_med(ichi),'',cfchi_sim(ichi))
axis([0 748,0,1.1]);
set(gca,'ytick',[0,.5,1],'xtick',[140 260 380 500 620 ])
text(1,1.5,"D: Chimberas solar park, hourly CF",'horizontalalignment', 'left')
set(gca,'ylabel','CF')
set(gca,'xlabel','date (year 2016)')
set(gca,'xticklabel',datestr(tw(1)+[5 10 15 250 25 ],"%d/%b"),'xtick',[5 10 15 20 25 ]*24)

%datetick('x','dd-mmm','keeplimits')

h(5)=subplot(5,1,5);
plot(cfchi_med(ichi),'',cfchi_sim(ichi))
axis([0 748,1.1,2]);
set(gca,'xticklabel',[],'ytick',[],'xtick',[])
axis off
legend('measured','simulated')
%datetick('x','dd-mmm','keeplimits')

