%% Convert wind speed to power
%  This script converts wind speed to power by averaging wind power curves
% (VWFM method)
%
% Herein we create a wind park average load curve, that accounts for wind
% variations between individual turbines
%
%%

% Copyright (C) 2019 Tomás Guozden
% Copyright (C) 2019 Juan Pablo Carbajal
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

clear all
close all

pkg load statistics

%% Load data
% Load all the required data
%
fname = datafile ('wind_data_wrf.dat');
printf ('Loading %s\n', fname);
load (fname);

%%
% Wind turbine power curves
fname      = {'v112_martin.dat', 'v117_martin.dat', 'v126_martin.dat'};
fname      = datafile (fname);
nt         = numel (fname);          % number of turbine power curve classes
turbine_pc = cell(nt, 1);
for i=1:numel(fname)
  printf ('Loading %s\n', fname{i});
  turbine_pc{i} = load (fname{i});
end%for

% Wind speed
Wmax = 1.2 * max (seriew(:)); % Maximum windspeed from time series
dW   = 0.25;                  % Step in windspeed for aggregation of power curve
W    = (0:dW:Wmax).';
nW   = length (W);

% allocate
PC  = cell(nt, 1); % individual power curves as function handles
PCa = cell(nt, 1); % Aggregated power curves as polynomial for ppval

disp ('Aggregating power curves ...')
for it = 1:nt    % iterate over turbine power curves
    tic
    % Create power curve fuction from data
    pcdata      = turbine_pc{it};
    pcdata(:,2) = pcdata(:,2) / max (pcdata(:,2)); % normalize power to [0 1]
    polypc      = interp1 (pcdata(:,1), pcdata(:,2), 'linear', 'pp');
    % This is the individual power curve with v-cut given by data
    vcut   = max (pcdata(:,1));
    PC{it} = @(x) ifelse(x <= vcut, ppval (polypc, x), 0.0);
    % This aggregates individual PCs with a Guassian distribution
    % on their parameters and creates and inteprolating polynomial
    PCa{it} = interp1 (W, aggregatePC (W, PC{it}), 'spline', 'pp');
    toc
end%for over turbine power curves

figure (1), clf
  for i=1:nt
    subplot (nt, 1, i)
    hold on
    h = plot (W, PC{i}(W), '-', W, ppval (PCa{i}, W), '-');
    ylabel ('Normalized power')
    legend (h, {sprintf('type %d', i), 'aggregated'});
    if i == nt
      xlabel ('Wind speed [m/s]')
    end%if
    axis tight
  end%for

%%
%  convolution winspeed - averaged power curves
% select power curve based on local overall wind average proximity
% classes:
% wind speed average
% above  9.25 m/s -->  class 1
% below 8.00 m/s  -->  class 3
% in between      -->  class 2
wind_speed_class = [10; 8.5; 7.5];
[~, idx]         = min(abs (wind_speed_class - mean (seriew)));

% Evaluate aggregated power curves in wind speed time series
%abs_seriew = abs (seriew);    % seriew are positive!
[nT nserie] = size (seriew);
hourlyCF    = zeros (nT, nserie);
for ii = 1:nserie
  hourlyCF(:,ii) = ppval (PCa{idx(ii)}, seriew(:,ii));
end%for

powerCap = cell2mat (wind_sites(:,8));

metadata = units = struct();
metadata.hourlyCF = ['hourly capacity factor series for wind sites,' ...
                   'a two-year period starting 2015-01-01 00:00hs UTC-3.\n'
                   'Rows are samples and columns wind sites.'];
units.hourlyCF    = 'adimensional';

% Some values in powerCap are actually projected capacities (as of 2018).
% We consider them as installed because they are already committed.
metadata.powerCap = ['column vector containing the capacity installed' ...
                     ' at wind sites [in GW]. The length of this vector' 
                     ' matches the number of sites.'];
units.powerCap    = 'GW';

fname = datafile ('wind_cfs.mat');
save ('-v7', fname, 'hourlyCF', 'powerCap', 'metadata', 'units');
printf ('Data saved to %s\n', fname);
