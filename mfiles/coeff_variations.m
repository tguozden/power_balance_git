load('../data/optimcapacity_raw.mat') % load results of optimization



% these are three rows of the PSigma data for aproximately 3, 6 and 9 GW of added cap.
na=19;
nb=39;
nc=59;

inscurr=CF*powCap;
std(inscurr)/mean(inscurr)

ins1020=CF*powerCap_addit(na,:)';
std(ins1020)/mean(ins1020)

ins2030=CF*powerCap_addit(nb,:)';
std(ins2030)/mean(ins2030)

ins3040=CF*powerCap_addit(nc,:)';
std(ins3040)/mean(ins3040)

insopt=CF*optim_ren;
std(insopt)/mean(insopt)



inscurr=CF*powCap;
mean(inscurr)/sum(powCap)

ins1020=CF*powerCap_addit(na,:)';
mean(ins1020)/sum(powerCap_addit(na,:)')

ins2030=CF*powerCap_addit(nb,:)';
mean(ins2030)/sum(powerCap_addit(nb,:)')

ins3040=CF*powerCap_addit(nc,:)';
mean(ins3040)/sum(powerCap_addit(nc,:)')

insopt=CF*optim_ren;
mean(insopt)/sum(optim_ren)