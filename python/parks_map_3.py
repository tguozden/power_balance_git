#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Script para generar un basemap con puntos de entrada
#shapefiles can be downloaded from https://osf.io/4xf35/

from mpl_toolkits.basemap import Basemap
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import os


#Cargar coordenadas desde archivo de puntos
arch_puntos = '../data/parks_map_data.txt'
lats = np.genfromtxt(arch_puntos,usecols=[5])
lons = np.genfromtxt(arch_puntos,usecols=[6])
cap1 = np.genfromtxt(arch_puntos,usecols=[1])
cap2 = np.genfromtxt(arch_puntos,usecols=[2])
cap3 = np.genfromtxt(arch_puntos,usecols=[3])
cap4 = np.genfromtxt(arch_puntos,usecols=[4])

agr = np.genfromtxt(arch_puntos,usecols=[0],dtype='float')
agr = agr.astype(int)


#simbolos y colores a utilizar
sym = ['o','o']
col = ['b','gold']
labels = ['wind', 'solar']


#Shp provincias Argentina para graficar
arch_shp1 = '../data/ARG_adm1'


##Inicializar el plot
fig, (ax1,ax2) = plt.subplots(1, 2,figsize=(12,8),sharey=True)

minx = np.min(lons)-4.5
miny = np.min(lats)-5.5
maxx = np.max(lons)+5
maxy = np.max(lats)+4
miny=-56
minx=-75
maxx=-53
maxy=-21
meanx = np.mean([minx,maxx])
meany = np.mean([miny,maxy])
m = Basemap(ax=ax1,llcrnrlon=minx,llcrnrlat=miny,urcrnrlon=maxx,urcrnrlat=maxy,resolution='h',projection='tmerc',lon_0=meanx,lat_0=meany,epsg=4326) #Bariloche

#Añadir los shapefile
#m.readshapefile(arch_shp,'lagos',drawbounds=True, color='#0000FF', linewidth=1.5)
m.readshapefile(arch_shp1,'ARG_adm1',drawbounds=True, color='black', linewidth=1.5)



ax1.text(-74.5, -25.5, 'A: \nCurrent and \nprojected  \nfacilities', fontsize=15, weight = 'bold')
for c in range(len(lons)):
	grupo = agr[c]
	#print grupo
	globals()['x'+str(c)],globals()['y'+str(c)]=m(lons[c],lats[c])
	m.plot(globals()['x'+str(c)], globals()['y'+str(c)], sym[grupo-1], color=col[grupo-1], markersize=10, mew=2,label= '%02d'%grupo+'-'+labels[grupo-1], markeredgewidth=0.5,markeredgecolor='black')



#Añadir grilla
parallels = np.arange(-55,-20,10) #Bariloche
meridians = np.arange(minx,maxx,10) #Bariloche

#Cambiar en caso de no requerir la visualización en todos los ejes
#m.drawparallels(parallels,labels=[False,False,False,False])
#m.drawmeridians(meridians,labels=[False,False,False,False])

#Añadir título al mapa
#titulo = plt.title("Parques Eólicos Ofrecidos\n(GENREN, Renovar I y Renovar I.V.)".decode('utf-8'), y=1.08)
#Cambiar extensión del gráfico de salida para que no se superpongan las coordenadas de la grilla sobre el título ###World_Shaded_Relief ###ESRI_Imagery_World_2D
#m.arcgisimage(service='World_Shaded_Relief', xpixels = 1500, verbose= True)

m.plot(-62, -50, 'o', color = 'royalblue', markersize=12)
ax1.text(-61, -50.5, 'wind', fontsize=15, weight = 'bold')
m.plot(-62, -53, 'o', color = 'gold', markersize=12)
ax1.text(-61, -53.5, 'solar', fontsize=15, weight = 'bold')


#plt.legend(handles,labels,loc=4) #bbox_to_anchor = [0.95, 0.35])
#plt.legend(handles,labels, loc='lower right',numpoints=1) #bbox_to_anchor = [0.95, 0.35])
#fig.subplots_adjust(top=0.65)

############################################3
###############################################grafico siguiente




#simbolos y colores a utilizar
sym = ['o','o']
col = ['b','gold']


###########################################  ###########################################


m = Basemap(ax=ax2,llcrnrlon=minx,llcrnrlat=miny,urcrnrlon=maxx,urcrnrlat=maxy,resolution='h',projection='tmerc',lon_0=meanx,lat_0=meany,epsg=4326) #Bariloche

#Añadir los shapefile
m.readshapefile(arch_shp1,'ARG_adm1',drawbounds=True, color='black', linewidth=1.5)

nparques = ['Viento','sol']
ax2.text(-74.5, -25.5, 'B: \nCurrent and \nprojected \ncapacity', fontsize=15, weight = 'bold')
for c in range(len(lons)):
	grupo = agr[c]
	#print grupo
	globals()['x'+str(c)],globals()['y'+str(c)]=m(lons[c],lats[c])
	m.scatter(globals()['x'+str(c)], globals()['y'+str(c)], 2*cap1[c],alpha=0.5, color=col[grupo-1],edgecolor='black', label= nparques[grupo-1])

#Añadir grilla
parallels = np.arange(-55,-20,10) 
meridians = np.arange(minx,maxx,10)

l1 = plt.scatter([],[], s=50,c='Grey', edgecolors='none')
l2 = plt.scatter([],[], s=100, c='Grey', edgecolors='none')
l3 = plt.scatter([],[], s=500, c='Grey', edgecolors='none')
l4 = plt.scatter([],[], s=1000, c='Grey', edgecolors='none')

labels = ["50 MW", "100 MW", "500 MW"]  #, "1000 MW"]

leg = plt.legend([l1, l2, l3, l4], labels, scatterpoints=1,  frameon=False, labelspacing=1, loc='lower right',  prop={'weight': 'bold'})

fig.subplots_adjust(wspace=0.01) #Espacio entre los gráficos

#fig.text(0.5, 0.02, 'Longitude [°]'.decode('utf-8'), ha='center',fontsize=15)
#fig.text(0.08, 0.5, 'Latitude [°]'.decode('utf-8'), va='center', rotation='vertical',fontsize=15)
#plt.show()
fig.tight_layout()

#~ plt.show()
plt.savefig('../img/parks_map_3.png', dpi=300)
