# Folder for data

Do not add files in this folder to the repository.
This folder is just a place holder.

To run the scripts in this repository you will need the following files
in this folder:

Data available at https://osf.io/6npg3/ 
* `merra_data.dat`: 37 years of merra data for all sites analized in the publication
* `sun_data.dat`: solar power from solar sites, years 2015 and 2016
* `wind_data_wrf.dat`: WRF runs plus additional information of wind sites, years 2015 and 2016
* `ARG_admin1.dbf`, `ARG_admin1.shp`, `ARG_admin1.shx`: shape files for plotting maps

Following cannot be shared due to legal reasons, contact authors for further information
* `demand.dat`:  Argentinas SADI's demand, years 2015 and 2016
* `rawson_loma_15-16.dat`: power production series at Rawson and Loma Blanca wind parks
* `arauco_nov2016.dat`: power production series at Parque Arauco wind park
* `chimb_sim_med_15_16.dat`: power production series at Chimberas solar park

If you have any further questions concerning the dataset please contact us.

## Raw data analysis
The publication's results are obtained by processing pre-processed raw data.
The pre-processed data can be directly downloaded and the results reproduced
without much effort (See `README.md` file in the root folder).

Here you will leanr howe to generating the pre-procesed data.
For this you will have to download considerable amount of data, and you need to
create an account on the data provider's website.
You will also need to perform WRF runs and GSEE scripts to convert solar
irradiance to power.

### Raw MERRA-2 data
To get the raw MERRA-2 data you need a username USER and password PASSWRD (see [GES DISC](https://disc.gsfc.nasa.gov/) for details)

```bash
wget --load-cookies .urs_cookies --save-cookies .urs_cookies --user USER --password PASSWRD --auth-no-challenge=on --keep-session-cookies --content-disposition --directory-prefix=merra2 --input-file=merra2_filelist_1979-2015.txt
```

This can take long so you can do it in parallel with a command like

```bash
cat merra2_filelist_1979-2015.txt | parallel -j20 --gnu "wget --load-cookies .urs_cookies --save-cookies .urs_cookies --user USER --password PASSWRD --auth-no-challenge=on --keep-session-cookies --content-disposition --directory-prefix=merra2 {}"
```

The option `-j20` indicates that 20 processes should be used, adapt this number to your convenience.

### Downloading GFS data
...

### Running WRF
...

### Running GSEE
...
